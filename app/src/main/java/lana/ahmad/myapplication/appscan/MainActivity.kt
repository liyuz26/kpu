package lana.ahmad.myapplication.appscan

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnmasuk.setOnClickListener(this)
        btnkeluar.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnmasuk->{
                val intent = Intent(this,MasukActivity::class.java)
                startActivity(intent)
            }
            R.id.btnkeluar->{
                val intent = Intent(this,KeluarActivity::class.java)
                startActivity(intent)
            }
        }
    }
}
