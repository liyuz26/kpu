

package lana.ahmad.myapplication.appscan

import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_keluar.*


class KeluarActivity: AppCompatActivity(), View.OnClickListener {
//    val activity = this@KeluarActivity
    lateinit var KeluarAdapter : AdapterDataKeluar
    //lateinit var mediaHelper : MediaHelper
    val url = "http://10.212.230.143/aplikasi-surat/show_data.php"
    val ok = ""

    var daftarKeluar = mutableListOf<HashMap<String,String>>()
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.CrKeluar->{

            }
            R.id.BkKeluar->{

            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_keluar)
        KeluarAdapter = AdapterDataKeluar(daftarKeluar,this)
        LsKeluar.layoutManager = LinearLayoutManager(this)
        LsKeluar.adapter = KeluarAdapter
        CrKeluar.setOnClickListener(this)
        BkKeluar.setOnClickListener(this)
        //mediaHelper = MediaHelper(this)
    }
    override fun onStart() {
        super.onStart()
        showDataSuratKeluar()
    }

    fun showDataSuratKeluar(){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarKeluar.clear()
                Toast.makeText(this,"koneksi ke server (data_keluar)",Toast.LENGTH_SHORT).show()
                val jsonArray = JSONArray(response)
                for(x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var keluar = HashMap<String,String>()
                    keluar.put("id_surat_keluar",jsonObject.getString("id_surat_keluar"))
                    keluar.put("no_surat",jsonObject.getString("no_surat"))
                    keluar.put("tgl_kirim",jsonObject.getString("tgl_kirim"))
                    keluar.put("perihal",jsonObject.getString("perihal"))
                    daftarKeluar.add(keluar)
                }
                KeluarAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_SHORT).show()
            }){
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

}