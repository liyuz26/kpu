package lana.ahmad.myapplication.appscan


import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_keluar.*

class AdapterDataKeluar(val dataKeluar: List<HashMap<String,String>>, val KeluarActivity: KeluarActivity) : RecyclerView.Adapter<AdapterDataKeluar.HolderDataKeluar>()
    {
        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): AdapterDataKeluar.HolderDataKeluar {
            val v =
                LayoutInflater.from(parent.context).inflate(R.layout.row_keluar, parent, false)
            return HolderDataKeluar(v)
        }

        override fun getItemCount(): Int {
            return dataKeluar.size
        }

        override fun onBindViewHolder(holder: AdapterDataKeluar.HolderDataKeluar, position: Int) {
            val data = dataKeluar.get(position)
            holder.TxSuratKeluar.setText(data.get("id_surat_keluar"))
            holder.TxNk.setText(data.get("no_surat"))
            holder.TxTK.setText(data.get("tgl_kirim"))
            holder.TxPerihal.setText(data.get("perihal"))

            if (position.rem(2) == 0) {
                holder.CLayout.setBackgroundColor(Color.rgb(230, 245, 240))
            } else {
                holder.CLayout.setBackgroundColor(Color.rgb(255, 255, 245))
            }
            holder.CLayout.setOnClickListener(View.OnClickListener {
               KeluarActivity.EdKeluar.setText(data.get("id_surat_keluar"))
            })

        }

        inner class HolderDataKeluar(v: View) : RecyclerView.ViewHolder(v) {
            val TxSuratKeluar = v.findViewById<TextView>(R.id.TxSuratKeluar)
            val TxNk = v.findViewById<TextView>(R.id.TxNK)
            val TxTK = v.findViewById<TextView>(R.id.TxTK)
            val TxPerihal = v.findViewById<TextView>(R.id.TxPerihal)

            val CLayout = v.findViewById<ConstraintLayout>(R.id.Clayout)
        }
    }